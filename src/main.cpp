#include <archeus.hpp>

namespace vox {
    class Temp : public arc::State {
    public:
        Temp(){}
        ~Temp(){}

        void update(){}
        void render(){}
    };
}

namespace arc {
    State *initState(){
        return new vox::Temp;
    }
}
